import sqlite3


# Open database
conn = sqlite3.connect('NChas_2024_Scouting.db')

# Create tables
match_results_create = '''
    CREATE TABLE results
    (
        report_id INTEGER PRIMARY KEY,
        match_number INTEGER,
        team_number INTEGER,
        start_position TEXT,
        auto_leave INTEGER,
        auto_preload_scored INTEGER,
        auto_score_spkr INTEGER,
        auto_score_amp INTEGER,
        teleop_spkr INTEGER,
        teleop_amp INTEGER,
        teleop_spkr_miss INTEGER,
        teleop_amp_miss INTEGER,
        amplifications INTEGER,
        coopertition INTEGER,
        parked INTEGER,
        endgame_climb REAL,
        endgame_trap REAL,
        endgame_spotlight REAL,
        auto_note_A INTEGER,
        auto_note_B INTEGER,
        auto_note_C INTEGER,
        auto_note_D INTEGER,
        auto_note_E INTEGER,
        auto_note_F INTEGER,
        auto_note_G INTEGER,
        auto_note_H INTEGER,
        score_zone_1 INTEGER,
        score_zone_2 INTEGER,
        score_zone_3 INTEGER,
        score_zone_4 INTEGER,
        score_zone_5 INTEGER,
        score_zone_6 INTEGER,
        notes TEXT,
        auto_astop INTEGER,
        auto_flubbed INTEGER,
        tipped INTEGER,
        carded INTEGER,
        broke INTEGER,
        fouls INTEGER
    )
'''

conn.execute(match_results_create)

conn.close()