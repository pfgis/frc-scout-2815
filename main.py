from __future__ import print_function

import os
import os.path
import random

# from io import BytesIO
# from matplotlib.figure import Figure

from flask import Flask
from flask import flash
from flask import request
from flask import jsonify
from flask import redirect
from flask import url_for
from flask import *
from werkzeug.utils import secure_filename
from io import StringIO
import datetime
import csv
import sqlite3

from sql_data import SQL_Templates

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

app = Flask(__name__)
app.secret_key = 'frc2815'

this_folder = os.path.dirname(os.path.abspath(__file__))
current_score_db = os.path.join(this_folder, 'NChas_2024_Scouting.db')

def db_query(sql, db_file):
    print(sql)
    with sqlite3.connect(db_file) as conn:
        cur = conn.cursor()
        cur.execute(sql)
        data_returned = cur.fetchall()
    conn.close()
    return data_returned

def db_commit(sql, db_file):
    print(sql)
    with sqlite3.connect(db_file) as conn:
        try:
            cur = conn.cursor()
            cur.execute(sql)
            conn.commit()
            msg = 'handled record successfully'
        except Exception as err:
            msg = f'error occurred: {err}'
            conn.rollback()
    conn.close()
    print(msg)
    return msg

def summarize(score_data):
    summary = {}
    for record in score_data:
        match = record[1]
        team = record[2]
        if match not in summary:
            summary[match] = []
        summary[match].append(team)
    return(summary)

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file), 
                       os.path.relpath(os.path.join(root, file), 
                                       os.path.join(path, '..')))

sql = SQL_Templates()

all_teams = []
# anderson: [281, 342, 343, 1051, 1287, 1293, 1319, 1539, 1648, 2815, 2974, 3091, 3329, 3489, 3490, 3976, 4189, 4451, 4533, 5130, 6167, 6366, 6961, 7085, 7676, 8137, 8575, 8736, 9315, 9477, 9561, 9561, 9571]
# charleston: [281, 342, 343, 1051, 1102, 1287, 1293, 1319, 1539, 1758, 2815, 3489, 3490, 3976, 4533, 4071, 5130, 6167, 6366, 6910, 6925, 6961, 7085, 7104, 8137, 8575, 9086, 9315, 9571]

@app.route("/")
def scout_page():
    print('in scout page')
    return render_template('scout_page.html')

@app.route("/confirmed")
def confirmation_page():
    print('confirming data received')
    photo_list = ['alex', 'brooks', 'ella', 'jeremiah', 'tyra']
    photo_id = random.randrange(0, len(photo_list))
    photo_name = f'{photo_list[photo_id]}.png'
    return render_template(
        'confirm_page.html',
        photo_name=photo_name
    )

@app.route("/admin/check_data")
def admin_check_data():
    score_data = db_query(sql.get_short_score, current_score_db)
    # print(score_data)
    match_summary = summarize(score_data)
    return render_template(
        'admin_light.html',
        score_data=score_data,
        match_summary=match_summary
    )

@app.route("/admin/full_data")
def admin_show_all_data():
    score_data = db_query(sql.get_full_score, current_score_db)
    if len(score_data) > 0:
        print(score_data[0])
    return render_template(
        'admin_export.html',
        score_data=score_data
    )

@app.route("/view_team/<team_id>")
def admin_show_team_data(team_id):
    team_scores = db_query(sql.get_team_scores(team_id), current_score_db)
    for score in team_scores:
        print(score)
    return render_template(
        'view_team.html',
        team_scores=team_scores
    )

@app.route("/admin/export_to_csv", methods = ['POST', 'GET'])
def admin_export_data():
    min_match_id = request.form['min_match_id'] if 'min_match_id' in request.form else 0
    if not min_match_id:
        min_match_id = 0
    timestamp = datetime.datetime.now().strftime('%y%m%d-%H%M')
    export_filename = f'score_export_{timestamp}.csv'
    si = StringIO()
    cw = csv.writer(si)
    conn = sqlite3.connect(current_score_db)
    cursor = conn.cursor()
    sql_string = sql.get_csv_score(min_match_id)
    print(sql_string)
    cursor.execute(sql_string)
    cw.writerow([i[0] for i in cursor.description])
    cw.writerows(cursor)
    conn.close()
    output = make_response(si.getvalue())
    output.headers["Content-Disposition"] = f"attachment; filename={export_filename}"
    output.headers["Content-type"] = "text/csv"
    return output

@app.route("/admin/see_graphs")
def see_graphs():
    fig = Figure()
    ax = fig.subplots()
    ax.plot([1, 2])
    # Save it to a temporary buffer.
    buf = BytesIO()
    fig.savefig(buf, format="png")
    # Embed the result in the html output.
    data = base64.b64encode(buf.getbuffer()).decode("ascii")
    return f"<img src='data:image/png;base64,{data}'/>"

@app.route('/scout/add_data', methods = ['POST', 'GET'])
def add_new():
    if request.method == 'POST':
        print(request.form)

        scout_data = {
            'match_number': request.form['match_number'],
            'team_number': request.form['team_number'],
            'start_position': request.form['start_position'],
            'auto_leave': 1 if 'auto_leave' in request.form else 0,
            'auto_preload_scored': 1 if 'auto_preload_scored' in request.form else 0,
            'auto_score_spkr': request.form['auto_score_spkr'],
            'auto_score_amp': request.form['auto_score_amp'],
            'teleop_spkr': request.form['teleop_spkr'],
            'teleop_amp': request.form['teleop_amp'],
            'teleop_spkr_miss': request.form['teleop_spkr_miss'],
            'teleop_amp_miss': request.form['teleop_amp_miss'],
            'amplifications': request.form['amplifications'],
            'coopertition': 1 if 'coopertition' in request.form else 0,
            'parked': 1 if 'parked' in request.form else 0,
            'endgame_climb': request.form['endgame_climb'] if 'endgame_climb' in request.form else 0,
            'endgame_trap': request.form['endgame_trap'] if 'endgame_trap' in request.form else 0,
            'endgame_spotlight': request.form['endgame_spotlight'] if 'endgame_spotlight' in request.form else 0,
            'auto_note_A': 1 if 'auto_note_A' in request.form else 0,
            'auto_note_B': 1 if 'auto_note_B' in request.form else 0,
            'auto_note_C': 1 if 'auto_note_C' in request.form else 0,
            'auto_note_D': 1 if 'auto_note_D' in request.form else 0,
            'auto_note_E': 1 if 'auto_note_E' in request.form else 0,
            'auto_note_F': 1 if 'auto_note_F' in request.form else 0,
            'auto_note_G': 1 if 'auto_note_G' in request.form else 0,
            'auto_note_H': 1 if 'auto_note_H' in request.form else 0,
            'score_zone_1': 1 if 'score_zone_1' in request.form else 0,
            'score_zone_2': 1 if 'score_zone_2' in request.form else 0,
            'score_zone_3': 1 if 'score_zone_3' in request.form else 0,
            'score_zone_4': 1 if 'score_zone_4' in request.form else 0,
            'score_zone_5': 1 if 'score_zone_5' in request.form else 0,
            'score_zone_6': 1 if 'score_zone_6' in request.form else 0,
            'notes': request.form['notes'],
            'auto_astop': 1 if 'auto_astop' in request.form else 0,
            'auto_flubbed': 1 if 'auto_flubbed' in request.form else 0,
            'tipped': 1 if 'tipped' in request.form else 0,
            'carded': 1 if 'carded' in request.form else 0,
            'broke': 1 if 'broke' in request.form else 0,
            'fouls': request.form['fouls']
        }

        db_commit(sql.enter_new_scout_record(scout_data), current_score_db)
        print(f'successfully added scouting report')
    return redirect('/confirmed')

if __name__ == "__main__":
    app.run(host="localhost", port=8080, debug=True)