class SQL_Templates():
    def __init__(self):
        print('init templates')

        self.get_short_score = "SELECT report_id, match_number, team_number FROM results ORDER BY match_number DESC"
        self.get_full_score = "SELECT match_number, team_number, auto_score_spkr, auto_score_amp, teleop_spkr, teleop_amp, endgame_climb FROM results ORDER BY match_number, team_number"

    def get_csv_score(self, main_match_id):
        return f"SELECT team_number,report_id,match_number,start_position,auto_leave,auto_preload_scored,auto_score_spkr,auto_score_amp,teleop_spkr,teleop_amp,teleop_spkr_miss,teleop_amp_miss,amplifications,coopertition,parked,endgame_climb,endgame_trap,endgame_spotlight,auto_note_A,auto_note_B,auto_note_C,auto_note_D,auto_note_E,auto_note_F,auto_note_G,auto_note_H,score_zone_1,score_zone_2,score_zone_3,score_zone_4,score_zone_5,score_zone_6,notes,auto_astop,auto_flubbed,tipped,carded,broke,fouls FROM results WHERE match_number >= {main_match_id} ORDER BY match_number, team_number"

    def get_team_scores(self, team_number):
        return f"SELECT * FROM results WHERE team_number={team_number}"

    def enter_new_scout_record(self, scout_data):
        return f"""
            INSERT INTO results (
                match_number,
                team_number,
                start_position,
                auto_leave,
                auto_preload_scored,
                auto_score_spkr,
                auto_score_amp,
                teleop_spkr,
                teleop_amp,
                teleop_spkr_miss,
                teleop_amp_miss,
                amplifications,
                coopertition,
                parked,
                endgame_climb,
                endgame_trap,
                endgame_spotlight,
                auto_note_A,
                auto_note_B,
                auto_note_C,
                auto_note_D,
                auto_note_E,
                auto_note_F,
                auto_note_G,
                auto_note_H,
                score_zone_1,
                score_zone_2,
                score_zone_3,
                score_zone_4,
                score_zone_5,
                score_zone_6,
                notes,
                auto_astop,
                auto_flubbed,
                tipped,
                carded,
                broke,
                fouls
            )
            VALUES (
                "{scout_data['match_number']}",
                "{scout_data['team_number']}",
                "{scout_data['start_position']}",
                "{scout_data['auto_leave']}",
                "{scout_data['auto_preload_scored']}",
                "{scout_data['auto_score_spkr']}",
                "{scout_data['auto_score_amp']}",
                "{scout_data['teleop_spkr']}",
                "{scout_data['teleop_amp']}",
                "{scout_data['teleop_spkr_miss']}",
                "{scout_data['teleop_amp_miss']}",
                "{scout_data['amplifications']}",
                "{scout_data['coopertition']}",
                "{scout_data['parked']}",
                "{scout_data['endgame_climb']}",
                "{scout_data['endgame_trap']}",
                "{scout_data['endgame_spotlight']}",
                "{scout_data['auto_note_A']}",
                "{scout_data['auto_note_B']}",
                "{scout_data['auto_note_C']}",
                "{scout_data['auto_note_D']}",
                "{scout_data['auto_note_E']}",
                "{scout_data['auto_note_F']}",
                "{scout_data['auto_note_G']}",
                "{scout_data['auto_note_H']}",
                "{scout_data['score_zone_1']}",
                "{scout_data['score_zone_2']}",
                "{scout_data['score_zone_3']}",
                "{scout_data['score_zone_4']}",
                "{scout_data['score_zone_5']}",
                "{scout_data['score_zone_6']}",
                "{scout_data['notes']}",
                "{scout_data['auto_astop']}",
                "{scout_data['auto_flubbed']}",
                "{scout_data['tipped']}",
                "{scout_data['carded']}",
                "{scout_data['broke']}",
                "{scout_data['fouls']}"
            )
        """